This app is pre-setup with an admin account. The initial credentials are:

**Username**: root<br/>
**Password**: ChangeMe1<br/>
**Email**: admin@cloudron.local<br/>

<sso>
The `root` user can login to GitLab using the `General` tab. Cloudron users can
login to GitLab using the `Cloudron` tab in the login screen.
</sso>

