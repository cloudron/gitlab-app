FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

# Go (https://golang.org/dl/)
ARG GOVERSION=1.22.3
ENV GOROOT /usr/local/go-${GOVERSION}
ENV PATH $GOROOT/bin:$PATH
RUN mkdir -p /usr/local/go-${GOVERSION} && \
    curl -L https://storage.googleapis.com/golang/go${GOVERSION}.linux-amd64.tar.gz | tar zxf - -C /usr/local/go-${GOVERSION} --strip-components 1

# the gitaly build unsets GOROOT for some reason
RUN ln -sf /usr/local/go-${GOVERSION} /usr/local/go

RUN apt-get -y update && \
    apt -y install nodejs ruby3.2 ruby3.2-dev && \
    rm -rf /etc/ssh_host_* && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Ubuntu 22 has private home directories by default (https://discourse.ubuntu.com/t/private-home-directories-for-ubuntu-21-04-onwards/)
RUN sed -e 's/^HOME_MODE\([[:space:]]\+\).*$/HOME_MODE\10755/' -i /etc/login.defs

RUN useradd --comment "GitLab" --create-home --shell /bin/bash git
RUN passwd -d git # by default, git account is created as inactive which prevents login via openssh. this disables password for account

RUN gem install --no-document bundler

USER git
# Docker doesn't set the HOME automatically based on USER
ENV HOME /home/git
RUN git config --global user.name "GitLab"
RUN git config --global user.email "git@gitlab.com"
RUN git config --global core.autocrlf input

## get gitlab
# renovate: datasource=gitlab-tags depName=gitlab-org/gitlab-foss versioning=semver extractVersion=^v(?<version>.+)$ registryUrl=https://gitlab.com
ARG GITLAB_VERSION=17.9.1

RUN mkdir /home/git/gitlab
RUN curl -L "https://gitlab.com/gitlab-org/gitlab-foss/-/archive/v${GITLAB_VERSION}/gitlab-foss-v${GITLAB_VERSION}.tar.gz" | tar -zxf - --strip=1 -C /home/git/gitlab
WORKDIR /home/git/gitlab
RUN bundle config set deployment true && \
    bundle config set without 'development test mysql aws krb5' && \
    bundle install
RUN npm install -g yarn && \
    yarn install --production --pure-lockfile

# gitlab-workhorse
RUN cd /home/git/gitlab/workhorse && make

## get gitlab-shell
RUN mkdir /home/git/gitlab-shell
RUN SHELL_VERSION=$(cat /home/git/gitlab/GITLAB_SHELL_VERSION) && \
    echo "Shell version is ${SHELL_VERSION}" && \
    curl -L "https://gitlab.com/gitlab-org/gitlab-shell/-/archive/v${SHELL_VERSION}/gitlab-shell-v${SHELL_VERSION}.tar.gz" | tar -zxf - --strip=1 -C /home/git/gitlab-shell
RUN ln -s /run/gitlab/ssh /home/git/.ssh
RUN ln -s /run/gitlab/gitlab-shell.yml /home/git/gitlab-shell/config.yml
RUN cd /home/git/gitlab-shell && \
    bundle config set --local deployment 'true' && \
    bundle config set --local with 'development test' && \
    bundle install && \
    make compile

# get gitaly. also install git from gitaly as per https://github.com/gitlabhq/gitlabhq/blob/master/doc/install/installation.md#software-requirements
RUN mkdir /home/git/gitaly
RUN GITALY_VERSION=$(cat /home/git/gitlab/GITALY_SERVER_VERSION) && \
    echo "Gitaly version is ${GITALY_VERSION}" && \
    curl -L "https://gitlab.com/gitlab-org/gitaly/-/archive/v${GITALY_VERSION}/gitaly-v${GITALY_VERSION}.tar.gz" | tar -zxf - --strip=1 -C /home/git/gitaly
RUN cd /home/git/gitaly && make PREFIX=/home/git/gitaly install && make git GIT_PREFIX=/home/git/gitaly_git

## compile assets (dummy configs are required). this step require gitlab-shell/VERSION to exist
RUN cp /home/git/gitlab/config/database.yml.postgresql /home/git/gitlab/config/database.yml && \
    yq eval -i "del(.*.geo)" /home/git/gitlab/config/database.yml && \
    yq eval -i "del(.*.embedding)" /home/git/gitlab/config/database.yml && \
    cp /home/git/gitlab/config/gitlab.yml.example /home/git/gitlab/config/gitlab.yml && \
    cp /home/git/gitlab/config/resque.yml.example /home/git/gitlab/config/resque.yml && \
    bundle exec rake gitlab:assets:compile NODE_ENV=production NODE_OPTIONS='--max-old-space-size=4096' RAILS_ENV=production SETUP_DB=false USE_DB=false SKIP_STORAGE_VALIDATION=true NO_SOURCEMAPS=true && \
    rm /home/git/gitlab/config/database.yml /home/git/gitlab/config/gitlab.yml /home/git/gitlab/config/resque.yml

# RUN sed -e "s/# config.logger.*/config.logger = Logger.new(STDOUT)/" -i /home/git/gitlab/config/environments/production.rb
# builds is the build logs and not the artifacts of the builds
RUN rm -rf /home/git/gitlab/log && ln -s /run/gitlab/log /home/git/gitlab/log && \
    rm -rf /home/git/gitlab/builds && ln -s /run/gitlab/builds /home/git/gitlab/builds

# hack to make structure.sql writable by active record
RUN cp /home/git/gitlab/db/structure.sql /home/git/gitlab/db/structure_original.sql && \
    ln -sf /run/gitlab/structure.sql /home/git/gitlab/db/structure.sql

# gitlab:setup rake task drops the database that is not allowed in cloudron
RUN sed '/"db:reset"/d' -i /home/git/gitlab/lib/tasks/gitlab/setup.rake

## Configuration
RUN cp /home/git/gitlab/config/puma.rb.example /home/git/gitlab/config/puma.rb
RUN ln -s /run/gitlab/config/database.yml /home/git/gitlab/config/database.yml && \
    ln -s /run/gitlab/config/resque.yml /home/git/gitlab/config/resque.yml && \
    ln -s /app/data/gitlab.yml /home/git/gitlab/config/gitlab.yml && \
    ln -s /run/gitlab/config/initializers/smtp_settings.rb /home/git/gitlab/config/initializers/smtp_settings.rb && \
    ln -sf /app/data/secrets.yml /home/git/gitlab/config/secrets.yml && \
    ln -s /run/gitlab/config/cable.yml /home/git/gitlab/config/cable.yml

# config/initializers/gitlab_shell_secret_token.rb creates a symlink, so this has to exist
RUN ln -sf /run/gitlab/gitlab_shell_secret /home/git/gitlab-shell/.gitlab_shell_secret && \
    ln -sf /run/gitlab/gitlab_workhorse_secret /home/git/gitlab/.gitlab_workhorse_secret

## Setup tmp
RUN mv /home/git/gitlab/tmp /home/git/gitlab/tmp_original && \
    ln -s /run/gitlab/tmp /home/git/gitlab/tmp

## used for generating tarball
RUN ln -s /tmp/gitlab /home/git/gitlab/shared/cache

RUN rm -rf /home/git/gitlab/public/uploads && ln -s /app/data/uploads /home/git/gitlab/public/uploads

# Add configuration template
ADD config_templates /home/git/config_templates

# add nginx config
USER root
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
RUN ln -s /etc/nginx/sites-available/gitlab /etc/nginx/sites-enabled/gitlab
RUN cp /home/git/gitlab/lib/support/nginx/gitlab /etc/nginx/sites-available/gitlab
RUN sed -e '/YOUR_SERVER_FQDN/d' \
        -e 's,.*access_log.*,access_log /dev/stdout;,' \
        -e '/server_tokens off;/a client_max_body_size 0;' \
        -e 's,.*error_log.*,error_log /dev/stderr info;,' \
        -e 's,.*proxy_set_header.*X-Forwarded-Proto.*$scheme;,proxy_set_header X-Forwarded-Proto https; proxy_set_header X-Forwarded-Ssl on;,' \
        -i /etc/nginx/sites-available/gitlab

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/gitlab/supervisord.log /var/log/supervisor/supervisord.log

ADD start.sh /home/git/start.sh

CMD [ "/home/git/start.sh" ]

