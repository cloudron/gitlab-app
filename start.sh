#!/bin/bash

set -eu

# SSH_PORT can be unset to disable SSH
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "==> SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
fi

# builds is the build logs and not artificats
mkdir -p /run/nginx /run/gitlab/ssh /run/gitlab/config/initializers /run/gitlab/log /run/gitlab/tmp /tmp/gitlab/repositories /run/gitlab/builds /run/sshd

mkdir -p /app/data/repositories /app/data/uploads /app/data/gitlab-satellites /app/data/backups/ /app/data/shared

# recreate the shared directory structure
cd /home/git/gitlab/shared && find . -type d -exec mkdir -p -- /app/data/shared/{} \;

cp /home/git/config_templates/database.yml /run/gitlab/config/database.yml
for kind in main ci; do
    yq eval -i ".production.${kind}.database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" /run/gitlab/config/database.yml
    yq eval -i ".production.${kind}.username=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" /run/gitlab/config/database.yml
    yq eval -i ".production.${kind}.password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" /run/gitlab/config/database.yml
    yq eval -i ".production.${kind}.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" /run/gitlab/config/database.yml
    yq eval -i ".production.${kind}.port=\"${CLOUDRON_POSTGRESQL_PORT}\"" /run/gitlab/config/database.yml
done

cp /home/git/config_templates/resque.yml /run/gitlab/config/resque.yml
yq eval -i ".production.url=\"${CLOUDRON_REDIS_URL}\"" /run/gitlab/config/resque.yml

[[ ! -f /app/data/gitlab.yml ]] && cp /home/git/config_templates/gitlab.yml /app/data/gitlab.yml

yq eval -i ".production.gitlab.host=\"${CLOUDRON_APP_DOMAIN}\"" /app/data/gitlab.yml
yq eval -i ".production.gitlab.email_from=\"${CLOUDRON_MAIL_FROM}\"" /app/data/gitlab.yml
yq eval -i ".production.gitlab.email_display_name=\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-GitLab}\"" /app/data/gitlab.yml
yq eval -i ".production.gitlab.email_reply_to=\"${CLOUDRON_MAIL_FROM}\"" /app/data/gitlab.yml
yq eval -i ".production.gitlab_shell.ssh_port=\"${SSH_PORT}\"" /app/data/gitlab.yml

[[ ! -f /app/data/gitaly.toml ]] && cp /home/git/config_templates/gitaly.toml /app/data/gitaly.toml

cp /home/git/config_templates/cable.yml /run/gitlab/config/cable.yml
yq eval -i ".production.url=\"${CLOUDRON_REDIS_URL}/0\"" /run/gitlab/config/cable.yml

# to be removed on the next release
migrate_ldap_users_to_oidc() {
    usersTableExists=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -AXqtc "SELECT count(*) FROM information_schema.tables WHERE table_schema LIKE 'public' AND table_type LIKE 'BASE TABLE' AND table_name = 'users'")
    if [ "${usersTableExists:-}" -eq 0 ]; then
        return 0;
    fi

    ldapUsers=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -AXqtc "SELECT count(*) FROM users u WHERE NOT EXISTS (SELECT 1 FROM identities ii WHERE ii.provider='openid_connect' AND ii.user_id=u.id) AND EXISTS (SELECT 1 FROM identities ii WHERE ii.provider='ldapmain' AND ii.user_id=u.id)")
    if [ "${ldapUsers:-}" -eq 0 ]; then
        return 0;
    fi

    echo "==> Migrate LDAP users to OIDC"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "INSERT INTO public.identities (extern_uid, provider, user_id, created_at, updated_at) SELECT uu.username, 'openid_connect' AS provider, i.user_id, NOW() AS created_at, NOW() AS updated_at  FROM public.identities i JOIN (SELECT u.id, u.username FROM users u WHERE NOT EXISTS (SELECT 1 FROM identities ii WHERE ii.provider='openid_connect' AND ii.user_id=u.id) AND EXISTS (SELECT 1 FROM identities ii WHERE ii.provider='ldapmain' AND ii.user_id=u.id)) uu ON uu.id=i.user_id"
}

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    # LDAP should be removed on the next release
    echo "==> Disabling LDAP integration"
    yq eval -i "del(.production.ldap)" /app/data/gitlab.yml
    migrate_ldap_users_to_oidc

    echo "==> Configuring OIDC auth"
    yq eval -i ".production.omniauth.enabled=true" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.allow_single_sign_on=[\"saml\", \"openid_connect\"]" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.block_auto_created_users=false" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.auto_link_ldap_user=true" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.auto_link_saml_user=false" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].name=\"openid_connect\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].label=\"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.name=\"openid_connect\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.scope=[\"openid\", \"email\", \"profile\"]" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.response_type=\"code\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.issuer=\"${CLOUDRON_OIDC_ISSUER}\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.discovery=true" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.uid_field=\"preferred_username\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.client_auth_method=\"query\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.send_scope_to_token_endpoint=false" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.pkce=true" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.client_options.identifier=\"${CLOUDRON_OIDC_CLIENT_ID}\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.client_options.secret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" /app/data/gitlab.yml
    yq eval -i ".production.omniauth.providers[0].args.client_options.redirect_uri=\"${CLOUDRON_APP_ORIGIN}/users/auth/openid_connect/callback\"" /app/data/gitlab.yml
fi

# https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/administration/incoming_email.md#email-sub-addressing
if [[ -n "${CLOUDRON_MAIL_IMAP_SERVER:-}" ]]; then
    echo "==> Enabling recvmail integration"

    yq eval -i ".production.incoming_email.enabled=true" /app/data/gitlab.yml
    tagged_address=$(echo "${CLOUDRON_MAIL_IMAP_USERNAME}" | sed -e 's/\(.*\)@/\1+%{key}@/')
    yq eval -i ".production.incoming_email.address=\"${tagged_address}\"" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.user=\"${CLOUDRON_MAIL_IMAP_USERNAME}\"" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.password=\"${CLOUDRON_MAIL_IMAP_PASSWORD}\"" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.host=\"${CLOUDRON_MAIL_IMAP_SERVER}\"" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.port=${CLOUDRON_MAIL_IMAP_PORT}" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.ssl=false" /app/data/gitlab.yml
    yq eval -i ".production.incoming_email.start_tls=false" /app/data/gitlab.yml
fi

# this also handles the case where the user does not use recvmail addon but external imap server
if [[ "$(yq eval '.production.incoming_email.enabled' /app/data/gitlab.yml)" == "true" ]]; then
    echo "==> Enabling mailroom (incoming email enabled)"
    export MAIL_ROOM_ENABLED=true # used in supervisor config
elif [[ "$(yq eval '.production.service_desk_email.enabled' /app/data/gitlab.yml)" == "true" ]]; then
    echo "==> Enabling mailroom (service desk enabled)"
    export MAIL_ROOM_ENABLED=true # used in supervisor config
else
    export MAIL_ROOM_ENABLED=false # used in supervisor config
fi

sed -e "s/##MAIL_SMTP_SERVER/${CLOUDRON_MAIL_SMTP_SERVER}/g" \
    -e "s/##MAIL_SMTP_PORT/${CLOUDRON_MAIL_SMTP_PORT}/g" \
    -e "s/##MAIL_SMTP_USERNAME/${CLOUDRON_MAIL_SMTP_USERNAME}/g" \
    -e "s/##MAIL_SMTP_PASSWORD/${CLOUDRON_MAIL_SMTP_PASSWORD}/g" \
    -e "s/##MAIL_DOMAIN/${CLOUDRON_MAIL_DOMAIN}/g" \
    /home/git/config_templates/smtp_settings.rb > /run/gitlab/config/initializers/smtp_settings.rb

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "==> Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "==> Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

# generate sshd config
sed -e "s/^Port .*/Port ${SSH_PORT}/" /home/git/config_templates/sshd_config > /run/gitlab/sshd_config

# https://docs.gitlab.com/charts/installation/secrets.html
echo "==> Fixing secrets"
if [[ -f /app/data/secrets.yml ]]; then
    if [[ "$(yq eval '.production.secret_key_base' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.secret_key_base=\"$(pwgen -1cns 128)\"" /app/data/secrets.yml
    fi
    if [[ "$(yq eval '.production.otp_key_base' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.otp_key_base=\"$(pwgen -1cns 128)\"" /app/data/secrets.yml
    fi
    if [[ "$(yq eval '.production.openid_connect_signing_key' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.openid_connect_signing_key=\"$(openssl genrsa 2048)\"" /app/data/secrets.yml
    fi
    if [[ "$(yq eval '.production.active_record_encryption_primary_key' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.active_record_encryption_primary_key=\"$(pwgen -1cns 32)\"" /app/data/secrets.yml
    fi
    if [[ "$(yq eval '.production.active_record_encryption_deterministic_key' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.active_record_encryption_deterministic_key=\"$(pwgen -1cns 32)\"" /app/data/secrets.yml
    fi
    if [[ "$(yq eval '.production.active_record_encryption_key_derivation_salt' /app/data/secrets.yml)" == "null" ]]; then
        yq eval -i ".production.active_record_encryption_key_derivation_salt=\"$(pwgen -1cns 32)\"" /app/data/secrets.yml
    fi

    cp /app/data/secrets.yml /run/gitlab/config/secrets.yml
else
    # even though the config file says min is 30, it should be atleast 32 (see #9)
    cp /home/git/config_templates/secrets.yml /run/gitlab/config/secrets.yml
    yq eval -i ".production.db_key_base=\"$(pwgen -1cns 64)\"" /run/gitlab/config/secrets.yml
    yq eval -i ".production.secret_key_base=\"$(pwgen -1cns 128)\"" /run/gitlab/config/secrets.yml
    yq eval -i ".production.otp_key_base=\"$(pwgen -1cns 128)\"" /run/gitlab/config/secrets.yml
    yq eval -i ".production.openid_connect_signing_key=\"$(openssl genrsa 2048)\"" /run/gitlab/config/secrets.yml
    cp /run/gitlab/config/secrets.yml /app/data/secrets.yml
fi

echo "==> Initializing gitlab shell"
cp /home/git/config_templates/gitlab-shell.yml /run/gitlab/gitlab-shell.yml

echo "==> Copying schema file"
cp /home/git/gitlab/db/structure_original.sql /run/gitlab/structure.sql

echo "==> Setting up tmp"
cp -fr /home/git/gitlab/tmp_original/* /run/gitlab/tmp
# obsolete pids makes puma unhappy during restarts
rm -f /run/gitlab/tmp/pids/*

echo "==> Fixing permissions"
chown -R git:git /app/data /run/gitlab /tmp/gitlab
# https://blog.diacode.com/fixing-temporary-dir-problems-with-ruby-2
chmod o+t /tmp

echo "==> Creating pg_trgm extension"
if ! PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -q -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"; then
    echo "==> Failed to create extension pg_trgm"
    exit 1
fi

cd /home/git/gitlab
if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "==> Initializing db losing existing data"
    # the default gitlab:setup drops the database that is not allowed on Cloudron
    # https://stackoverflow.com/questions/10301794/difference-between-rake-dbmigrate-dbreset-and-dbschemaload
    echo "==> db setup: loading schema"
    echo "yes" | sudo -u git bundle exec rake db:schema:load RAILS_ENV=production force=yes DISABLE_DATABASE_ENVIRONMENT_CHECK=1
    echo "==> db setup: seeding database"
    echo "yes" | sudo -u git bundle exec rake db:seed RAILS_ENV=production force=yes

    # In GitLab 10.8 (bbccd310573), the setup rake task requires gitaly to be running (https://github.com/sameersbn/docker-gitlab/issues/1617)
    HOME=/tmp gosu git /home/git/gitaly/bin/gitaly /app/data/gitaly.toml &
    gitaly_pid=$!
    echo "==> db setup: waiting for gitaly to start"
    sleep 5

    # gitlab:setup has been patched to remove db:reset
    echo "==> db setup: setting up gitlab"
    echo "yes" | sudo -u git bundle exec rake gitlab:setup RAILS_ENV=production GITLAB_ROOT_PASSWORD=ChangeMe1 GITLAB_ROOT_EMAIL=admin@cloudron.local force=yes
    echo "==> db setup: stopping gitaly"
    kill -SIGTERM ${gitaly_pid} # this kills the process group
    echo "==> Running migration script"
    sudo -u git bundle exec rake db:migrate RAILS_ENV=production
    touch /app/data/.dbsetup
else
    echo "==> Upgrading existing db"
    sudo -u git bundle exec rake db:migrate RAILS_ENV=production
fi

# For some reason, this tries to create repo directory
echo "==> Installing gitlab shell"
chmod u+rwx,g=rx,o-rwx /app/data/gitlab-satellites
sudo -u git /home/git/gitlab-shell/support/make_necessary_dirs

# Recreate authorized_keys from database keys
echo "==> Synchronizing ssh keys"
echo "yes" | sudo -u git -H bundle exec rake gitlab:shell:setup RAILS_ENV=production

echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i GitLab
