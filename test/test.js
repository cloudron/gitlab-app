#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);
    let app, browser;
    const LOCATION = process.env.LOCATION || 'test';
    const repodir = '/tmp/testrepo';
    const reponame = 'testrepo';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const TIMEOUT = parseInt(process.env.TIMEOUT) || 40000;
    const gitlab_root = 'root';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    after(function () {
        browser.quit();
        fs.rmSync(repodir, { recursive: true, force: true });
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function adminLogin() {
        await browser.get(`https://${app.fqdn}/users/sign_in`);

        await waitForElement(By.id('user_login'));
        await browser.findElement(By.id('user_login')).sendKeys(gitlab_root);
        await browser.findElement(By.id('user_password')).sendKeys('ChangeMe1');
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[@data-testid="sign-in-button"]')).click();
        await waitForElement(By.xpath('//a[contains(@href, "/dashboard/todos")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/users/sign_in`);

        await waitForElement(By.xpath('//button[@data-testid="oidc-login-button"]'));
        await browser.findElement(By.xpath('//button[@data-testid="oidc-login-button"]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//a[contains(@href, "/dashboard/todos")]'));
    }

    async function checkSshCloneUrl() {
        await browser.get(`https://${app.fqdn}/${gitlab_root}/${reponame}`);
        await browser.findElement(By.xpath('//div[@data-testid="code-dropdown"]/button')).click();
        await waitForElement(By.id('copy-ssh-url-input'));
        const cloneUrl = await browser.findElement(By.id('copy-ssh-url-input')).getAttribute('value');
        expect(cloneUrl).to.be(`ssh://git@${app.fqdn}:30303/${gitlab_root}/${reponame}.git`);
    }

    async function checkHttpCloneUrl() {
        await browser.get(`https://${app.fqdn}/${gitlab_root}/${reponame}`);
        await browser.findElement(By.xpath('//div[@data-testid="code-dropdown"]/button')).click();
        await waitForElement(By.id('copy-http-url-input'));
        const cloneUrl = await browser.findElement(By.id('copy-http-url-input')).getAttribute('value');
        expect(cloneUrl).to.be(`https://${app.fqdn}/${gitlab_root}/${reponame}.git`);
    }

    async function checkClone() {
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`git clone ssh://git@${app.fqdn}:30303/${gitlab_root}/${reponame}.git ${repodir}`, { env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        fs.rmSync(repodir, { recursive: true, force: true });

        env.GIT_SSL_NO_VERIFY = '1';
        execSync(`git clone https://${gitlab_root}:${encodeURIComponent('ChangeMe1')}@${app.fqdn}/${gitlab_root}/${reponame}.git ${repodir}`, { env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        fs.rmSync(repodir, { recursive: true, force: true });
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[@data-testid="user-avatar-content"]'));
        await browser.findElement(By.xpath('//div[@data-testid="user-avatar-content"]')).click();
        await browser.findElement(By.xpath('//a[contains(@data-testid, "sign-out-link")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "GitLab Community Edition")]')), TIMEOUT);
    }

    async function addPublicKey() {
        await browser.get(`https://${app.fqdn}/-/user_settings/ssh_keys`);
        execSync(`chmod g-rw,o-rw ${__dirname}/id_ed25519`); // ssh will complain about perms later
        const publicKey = fs.readFileSync(__dirname + '/id_ed25519.pub', 'utf8');
        await browser.findElement(By.xpath('//button[contains(., "Add new key")]')).click();
        await waitForElement(By.id('key_key'));
        await browser.findElement(By.id('key_key')).sendKeys(publicKey);
        await browser.findElement(By.id('key_title')).sendKeys('testkey');
        await browser.sleep(4000);
        const button = browser.findElement(By.xpath('//button[@data-testid="add-key-button"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await button.click();

        await waitForElement(By.xpath('//h1[contains(., "testkey")]'));
    }

    async function createRepo() {
        await browser.get(`https://${app.fqdn}/projects/new#blank_project`);
        await waitForElement(By.id('project_name'));
        await browser.findElement(By.id('project_name')).sendKeys(reponame);
        await browser.findElement(By.xpath('//span[contains(@title, "Pick a group")]/ancestor::button')).click();
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//li[@data-testid="listbox-item-gid://gitlab/Namespaces::UserNamespace/1"]')).click();
        await browser.findElement(By.xpath('//label[@for="project_visibility_level_10"]')).click(); // visibility level to 'Internal'
        await browser.findElement(By.xpath('//label[@for="project_initialize_with_readme"]')).click(); // uncheck README creation
        const button = browser.findElement(By.xpath('//button[@data-testid="project-create-button"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await button.click();
        await browser.wait(until.elementLocated(By.xpath('//div[contains(text(), "was successfully created")]')), TIMEOUT);
    }

    async function addReadme() {
        const branch = 'main';
        await browser.get(`https://${app.fqdn}/${gitlab_root}/${reponame}/-/new/${branch}?file_name=README.md`);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//textarea')).sendKeys('This is a test document');
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[@data-testid="blob-edit-header-commit-button"]')).click();
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//textarea[@data-testid="commit-message-field"]')).sendKeys('This is a test message');
        await browser.findElement(By.xpath('//button[@data-testid="commit-change-modal-commit-button"]')).click();
        await browser.sleep(5000);
        await waitForElement(By.xpath('//div[contains(text(), "has been successfully created")]'));
    }

    async function forkRepo() {// /-/forks/new
        await browser.get(`https://${app.fqdn}/${gitlab_root}/${reponame}/-/forks/new`);
        await browser.sleep(10000);
        await browser.findElement(By.xpath('//span[contains(., "Select a namespace")]/ancestor::button')).click();
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//li[@data-testid="listbox-item-gid://gitlab/Namespaces::UserNamespace/3"]')).click();
        await browser.findElement(By.xpath('//button[@data-testid="fork-project-button"]')).click();
        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url.endsWith(`/${username}/${reponame}`);
            });
        }, TIMEOUT);
    }

    async function checkForkedRepo() {
        await browser.get(`https://${app.fqdn}/${username}/${reponame}`);
        await waitForElement(By.xpath(`//h1[contains(., "${reponame}")]`));
    }

    async function cloneUrl() {
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`git clone ssh://git@${app.fqdn}:30303/${gitlab_root}/${reponame}.git ${repodir}`, { env });
    }

    async function addFile() {
        const branch = 'main';
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':30303/' + gitlab_root + '/' + reponame + ' ' + branch,
            { env: env, cwd: repodir });
        fs.rmSync('/tmp/testrepo', { recursive: true, force: true });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app without sso', function () { execSync('cloudron install --no-sso -p SSH_PORT=30303 --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // SSO
    it('install app', function () { execSync('cloudron install -p SSH_PORT=30303 --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('displays correct ssh clone url', checkSshCloneUrl);
    it('displays correct https clone url', checkHttpCloneUrl);

    it('can add a README.md', addReadme);
    it('can clone the url', cloneUrl);
    it('can add and push a file', addFile);

    it('can logout', logout);

    it('can login via OIDC as cloudron user', loginOIDC.bind(null, username, password, false));
    it('can fork', forkRepo);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('clear cache', clearCache);
    it('can get app information', getAppInfo);
    it('can login via OIDC as cloudron user', loginOIDC.bind(null, username, password, false));
    it('can check forked repo', checkForkedRepo);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install -p SSH_PORT=30303 --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can clone the url', checkClone);

    it('move to different location', async function () {
        await browser.get('about:blank');
        browser.manage().deleteAllCookies();
        execSync('cloudron configure -p SSH_PORT=30303 --location ' + LOCATION + '2', EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('clear cache', clearCache);
    it('can admin login', adminLogin);
    it('displays correct ssh clone url', checkSshCloneUrl);
    it('displays correct http clone url', checkHttpCloneUrl);
    it('can logout', logout);

    it('can login via OIDC as cloudron user', loginOIDC.bind(null, username, password, false));

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync('cloudron install --appstore-id com.gitlab.cloudronapp -p SSH_PORT=30303 --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('clear cache', clearCache);
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('can add a README.md', addReadme);

    it('can clone the url', cloneUrl);
    it('can add and push a file', addFile);
    it('can logout', logout);

    it('can login via OIDC as cloudron user', loginOIDC.bind(null, username, password, false));
    it('can fork', forkRepo);
    it('can logout', logout);

    it('can update', async function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('clear cache', clearCache);
    it('can get app information', getAppInfo);
    it('can login via OIDC as cloudron user', loginOIDC.bind(null, username, password, false));
    it('can check forked repo', checkForkedRepo);
    it('can clone the url', checkClone);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
